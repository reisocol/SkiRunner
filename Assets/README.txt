README

Ski Runner
===========
It is a endless 3D runner game where its main objective is
to reach a maximum distance possible by skiing on the
Ski Resort.


Features
=========

Ski Locomotion
Collect coins
Endless world generator


Installation
=============

Install SkiRunner.apk in your mobile phone.


How to Play
============

Open SkiRunner.unity

Touch Start button to start playing the game.

When your players loses, you have the option to restart
or to exit the game.

Touch Restart button to play again.
Touch Exit button to quit the game.


Controls
=========

The game only support swipe gestures on mobiles.

Swipe Up to Jump
Swipe Right to ski to the right
Swipe Left to ski to the left


Third-Party Assets
===================
All assets were purchased or downloaded from Unity Asset Store.

Tree vertex color shaders (Check Chunk7 Tree shaders)
ProBuilder: https://assetstore.unity.com/packages/tools/modeling/probuilder-111418

Player, Snow Ski ways, Trees, Fences
Ski - Props for Winter Runner : https://assetstore.unity.com/packages/3d/characters/ski-props-for-winter-runner-68920

Props place holders
Low Poly World North : https://assetstore.unity.com/packages/3d/characters/low-poly-world-north-76705

HUDFPS.cs
FPS frame rate : http://wiki.unity3d.com/index.php?title=FramesPerSecond




Ski Running Tiers
==================

Tier 0
-------
Base Gameplay: Subway Surfers
Start - Gameplay - GameOver in one scene only

Tier 1
-------
Requirement 1:
3D world combining 3D and 2D assets - Ski Resort Logo at the game start.

Requirement 2:
Physics interaction: Player jump motion
Collision Detection: Obstacle and coins collisions
Particles: Snow Fall
Spawning System: Spawn Chunk system

Requirement 3:
Transparent Shader: Clouds

Tier 2
--------
Requirement 1:
None

Requirement 2:
Camera Movement: Game Start, Gameplay, Gameover

Tier 3
-------
Requirement 1:
None

Requirement 2:
Vertex color: Chunk 7 Trees
