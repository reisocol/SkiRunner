﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * StartAnimationState
 * It handles player mechanics and game values
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using SkiRunning.Base;
using System;
using UnityEngine;

public class StartAnimationState : GameAnimationState
{
    public float animationLength;

    public string paramName = "Start";

    public event Action OnAnimationInitialized;

    private void Start()
    {
        StartSki();
    }

    public void StartSki()
    {
        ChangeParameters(paramName, true);

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Start"))
            animationLength = animator.GetCurrentAnimatorStateInfo(0).length * 4f;
    }
}
