﻿using SkiRunner.Base;
using SkiRunner.Game;
using SkiRunner.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkiRunner.GameStates.Gameplay
{
    public class GameOverState : GameplayState
    {
        public bool moveCamera;


        private void Start()
        {
            Init();
        }

        //---------------------------
        // Init Game Over State
        //---------------------------

        public override void Init()
        {
            base.Init();

            moveCamera = true;

            GameManager.instance.mainCamera.transform.SetParent(null);

            Player.instance.playerPoints.canCount = false;

            GameManager.instance.gameOverText.gameObject.SetActive(true);
            GameManager.instance.restartButton.gameObject.SetActive(true);
            GameManager.instance.exitButton.gameObject.SetActive(true);
        }


        //---------------------------
        // Clean resources 
        //---------------------------

        public override void Clean()
        {
            base.Clean();

            Destroy(this);
        }

        // Call every frame
        private void Update()
        {
            if (moveCamera)
                GameManager.instance.mainCamera.transform.Translate(Vector3.back * Time.deltaTime);
        }
    }
}


