﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * IntroGameState
 * It handles gameplay intro initializations and setup
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using SkiRunner.Base;
using SkiRunner.Game;
using SkiRunner.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace SkiRunner.GameStates.Gameplay
{
    public class PlayingGameState : GameplayState
    {
        private void Start()
        {
            Init();

            Player.instance.OnPlayerDies += GameplayEnds;
        }


        //---------------------------
        // Write something here.
        //---------------------------

        public override void Init()
        {
            base.Init();

            // Hide Game Title text
            GameManager.instance.gameNameText.gameObject.SetActive(false);

            // Hide Game icon image
            GameManager.instance.iconImg.gameObject.SetActive(false);

            // initial values when the game starts
            Player.instance.lifeEnergy = 100;

            Player.instance.isAlive = true;
        }


        //---------------------------
        // Write something here.
        //---------------------------

        public override void Clean()
        {
            base.Clean();

            Player.instance.OnPlayerDies -= GameplayEnds;

            Player.instance.playerInput.canReadInput = false;

            Destroy(this);
        }

        public void GameplayEnds()
        {
            Player.instance.StopLocomotion();

            GameplayManager.instance.SetGameplayState(Common.CommonTypes.CurrentGameplayState.GameOver);
        }
    }
}


