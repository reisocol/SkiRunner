﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * Points 
 * It handles players points calculation and UI update
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using UnityEngine;
using UnityEngine.UI;

namespace SkiRunner.Game
{
    public class Points : MonoBehaviour
    {
        public Text pointsTxt;
        public Text coinsTxt;

        public int points;
        public int coins;

        public bool canCount;


        private void Start()
        {
            points = 0;
        }

        // Call every frame
        void Update()
        {
            if (canCount)
            {
                points += 1;

                UpdatePointsUI();
            }
        }


        //---------------------------
        // Write something here.
        //---------------------------

        public void StartCounting()
        {
            canCount = true;
        }

        //---------------------------
        // Write something here.
        //---------------------------

        void UpdatePointsUI()
        {
            pointsTxt.text = points.ToString();
            coinsTxt.text = coins.ToString();
        }
    }
}
