﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * Player
 * It handles player mechanics and game values
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using SkiRunner.Common;
using SkiRunner.Core;
using SkiRunner.GameInput;
using SkiRunner.Manager;
using SkiRunner.Mechanics.Locomotion;
using System;
using UnityEngine;

namespace SkiRunner.Game
{
    public class Player : Singleton<Player>
    {
        // player health
        public int lifeEnergy;

        // is player on the ground?
        public bool isGrounded;

        // is player alive?
        public bool isAlive;

        // locomotion component access
        public Locomotion playerLocomotion
        {
            get
            {
                return GetComponent<Locomotion>();
            }
        }

        // player input component access
        public PlayerInput playerInput
        {
            get
            {
                return GetComponent<PlayerInput>();
            }
        }

        // player's rigid body
        public Rigidbody rigidBody
        {
            get
            {
                return GetComponent<Rigidbody>();
            }
        }

        // player animation states
        public PlayerAnimationManager playerAnimation
        {
            get
            {
                return GetComponent<PlayerAnimationManager>();
            }
        }

        // player points component
        public Points playerPoints
        {
            get
            {
                return GetComponent<Points>();
            }
        }

        public event Action OnPlayerDies;

        // Use this for initialization
        void Start()
        {
            playerInput.OnSwipeDetected += GoToSide;
            playerInput.OnSwipeDetected += Jump;
        }

        // Update is called once per frame
        void Update()
        {
            
        }


        //-------------------------------------------------------
        // Enables first animation when player is speeding up
        //-------------------------------------------------------

        public void StartSkiing()
        {
            // chunk can start moving
            playerLocomotion.canLocomote = true;

            // make camera child of player
            GameManager.instance.mainCamera.parent = this.transform;

            // set start animation
            playerAnimation.SetPlayerAnimationState(CommonTypes.CurrentPlayerAnimation.Start);

            // wait to change animation after time
            WaitToCall(Skiing, 3.16f);
        }

        //-------------------------------------------------------
        // Gameplay main animation when the player is skiing
        //-------------------------------------------------------

        public void Skiing()
        {
            // set skiing animation
            playerAnimation.SetPlayerAnimationState(CommonTypes.CurrentPlayerAnimation.Skiing);

            // the gameplay starts here
            GameplayManager.instance.SetGameplayState(CommonTypes.CurrentGameplayState.Playing);

            // start reading input from player
            playerInput.canReadInput = true;

            // points start counting
            playerPoints.StartCounting();
        }


        //------------------------------------------------
        // Stop world chunks from moving
        //------------------------------------------------

        public void StopLocomotion()
        {
            playerLocomotion.canLocomote = false;
        }

        //-------------------------------------
        // Side translation of the player
        //-------------------------------------

        public void GoToSide()
        {
            if (playerInput.pressedLeft)
            {
                playerLocomotion.LocomoteToSides(Locomotion.SideDirection.Left);

                playerInput.pressedLeft = false;
            }
            else if (playerInput.pressedRight)
            {
                playerLocomotion.LocomoteToSides(Locomotion.SideDirection.Right);

                playerInput.pressedRight = false;
            }
        }


        //---------------------------
        // Player Jumps
        //---------------------------

        public void Jump()
        {
            if (playerInput.swipeUp)
                playerLocomotion.Jump();
        }

        // Check if player is on the ground
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.tag.Equals("Ground"))
            {
                if (GameplayManager.instance.gameState == Common.CommonTypes.CurrentGameplayState.Playing)
                {
                    isGrounded = true;

                    Player.instance.playerInput.canReadInput = true;
                }
            }

            // check if player collided with obstacles

            else if (collision.collider.tag.Equals("Obstacle"))
            {
                // lose half amount of energy when colliding
                lifeEnergy -= 50;

                if (lifeEnergy <= 0)
                {
                    isAlive = false;

                    // event triggers when player dies
                    if (OnPlayerDies != null)
                        OnPlayerDies();
                }
            }
        }

        private void OnCollisionExit(Collision collision)
        {
            isGrounded = false;
        }
    }
}


