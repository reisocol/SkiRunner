﻿
using SkiRunner.Core;
using SkiRunner.Interfaces;
using UnityEngine;

namespace SkiRunning.Base
{
    public class GameAnimationState : Singleton<GameAnimationState>, iAnimate
    {
        protected Animator animator
        {
            get
            {
                return GetComponent<Animator>();
            }
        }

        public void ChangeParameters(string param, bool condition)
        {
            animator.SetBool(param, condition);
        }

        public void Clean()
        {
            Destroy(this.gameObject);
        }
    }
}


