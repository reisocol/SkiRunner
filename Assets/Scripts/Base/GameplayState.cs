﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * GameplayState 
 * It handles game play state base functionality
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using UnityEngine;

namespace SkiRunner.Base
{
    public class GameplayState : MonoBehaviour
    {
        public bool stateCompleted;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public virtual void Init()
        {
            Debug.Log("Intro");
        }

        public virtual void Clean()
        {
            Debug.Log("Cleaning...");
        }
    }
}


