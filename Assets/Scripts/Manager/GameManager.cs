﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * GameManager 
 * It Handles game application initializations and dealocation of resources
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using SkiRunner.Core;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace SkiRunner.Manager
{
    public class GameManager : Singleton<GameManager>
    {
        public Transform mainCamera;

        // Intro assets
        public Text gameNameText;
        public Image iconImg;
        public Button startButton;

        //Game over assets
        public Text gameOverText;
        public Button restartButton;
        public Button exitButton;

        protected override void Awake()
        {
            base.Awake();
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void RestartGame()
        {
            SceneManager.LoadScene(0);
        }

        public void QuitApplication()
        {
            Application.Quit();
        }
    }
}


