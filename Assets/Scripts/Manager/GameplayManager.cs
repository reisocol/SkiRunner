﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * GameplayManager 
 * It handles game states that occur during gameplay
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using SkiRunner.Base;
using SkiRunner.Core;
using SkiRunner.GameStates.Gameplay;
using SkiRunner.Common;
using System;

namespace SkiRunner.Manager
{
    public class GameplayManager : Singleton<GameplayManager>
    {
        public CommonTypes.CurrentGameplayState gameState;

        // gameplay states
        GameplayState currentState;
        GameplayState previousState;

        // specific gameplay states
        IntroGameState introGameState;
        PlayingGameState playingGameState;

        // Gameplay events
        public event Action OnGameIntroEnds;
        public event Action OnGameplayEnds;
        public event Action OnGamePointsEnds;

        // Use this for initialization
        void Start()
        {
            // set gameplay to intro when pressed the start button
            GameManager.instance.startButton.onClick.AddListener(delegate { SetGameplayState(CommonTypes.CurrentGameplayState.Intro); });
        }

        //---------------------------
        // Write something here.
        //---------------------------

        public void SetGameplayState(CommonTypes.CurrentGameplayState state)
        {
            gameState = state;

            // if it is the first gameplay state delete start button
            if (previousState == null)
                GameManager.instance.startButton.gameObject.SetActive(false);
            else
                previousState.Clean();

            // set gameplay state
            GetGameplayState(state);
        }

        //---------------------------
        // Write something here.
        //---------------------------

        private void GetGameplayState(CommonTypes.CurrentGameplayState state)
        {
            // Add the component based on gameplay state
            switch(state)
            {
                case CommonTypes.CurrentGameplayState.Intro:
                    currentState = this.gameObject.AddComponent<IntroGameState>();
                    previousState = currentState;
                    break;
                case CommonTypes.CurrentGameplayState.Playing:
                    currentState = this.gameObject.AddComponent<PlayingGameState>();
                    previousState = currentState;
                    break;

                case CommonTypes.CurrentGameplayState.GameOver:
                    currentState = this.gameObject.AddComponent<GameOverState>();
                    previousState = currentState;
                    break;
            }
        }

    }
}


