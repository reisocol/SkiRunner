﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * Singleton
 * Pattern to access a single instance of a object when running the application
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using System;
using UnityEngine;

namespace SkiRunner.Core
{
    public class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        public static T instance;

        public static T Instance
        {
            get
            {
                instance = FindObjectOfType<T>();
                return instance;
            }
        }

        private Action action;

        protected virtual void Awake()
        {
            instance = (T)this;
        }

        public virtual void WaitToCall(Action method, float time)
        {
            this.action = method;

            Invoke("Call", time);
        }

        void Call()
        {
            action();
        }
       
    }
}


