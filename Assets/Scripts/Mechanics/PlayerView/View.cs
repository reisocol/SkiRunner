﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * View (Game Mechanics)
 * It handles the distance view calculation so that it generates a new chunk
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using SkiRunner.Core;
using SkiRunner.Game;
using UnityEngine;

namespace SkiRunner.Mechanics
{
    public class View : Singleton<View>
    {
        public Transform maximumView;

        public float distance;

        // Use this for initialization
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
           
        }


        //---------------------------
        // Write something here.
        //---------------------------

        void CalculateViewDistance()
        {
            distance = (Player.instance.transform.position - ChunkGenerator.instance.chunkSpawn.position).magnitude;
        }
    }
}


