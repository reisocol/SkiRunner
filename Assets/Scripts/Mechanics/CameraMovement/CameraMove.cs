﻿

/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * CameraMove 
 * It Handles game environment chunk that has items or obstacles and
 * gets generated when playing the game.
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using SkiRunner.Core;
using UnityEngine;

namespace SkiRunning.Mechanics.CameraMovement
{
    public class CameraMove : Singleton<CameraMove>
    {
        private float angle = 0f;
        private float radius = 1f;

        private float offsetX;
        private float offsetZ;

        private Vector3 newMoveVec;

        public Transform target;
        public Transform newMove;

        public bool startMove;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if (startMove)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, Time.deltaTime);
            }
        }
    }
}


