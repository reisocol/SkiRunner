﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * Locomotion (Game Mechanics)
 * It Handles how the player locomotes during gameplay
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */


using UnityEngine;
using SkiRunner.Game;
using System.Collections.Generic;

namespace SkiRunner.Mechanics.Locomotion
{
    public class Locomotion : MonoBehaviour
    {
        public enum SideDirection
        {
            Center=0,
            Right=1,
            Left=-1,
            None = 2,
        }

        // how fast player can locomote
        public float playerSpeed = 0f;
        public float sideSpeed;
        private float jumpForce = 300f;

        // Vector side positions
        public Vector3 sideLocomotion;
        public Vector3 limitLeft;
        public Vector3 limitRight;

        // hold Vector3 values based on side direction
        public Dictionary<int,Vector3> paths = new Dictionary<int, Vector3>();

        public int currentPath = 0;

        // Can the player locomote?
        public bool canLocomote;

        // Can the player jump?
        public bool canJump;

        // player can go to sides or reached limits?
        public bool canGoRight;
        public bool canGoLeft;

        // Use this for initialization
        void Start()
        {
            paths.Add((int)SideDirection.Center,transform.position);
            paths.Add((int)SideDirection.Left,transform.position - (Vector3.right * 4.9f));
            paths.Add((int)SideDirection.Right,transform.position + (Vector3.right * 4.9f));

            limitLeft = paths[(int)SideDirection.Left];
            limitRight = paths[(int)SideDirection.Right];
        }

        // Update is called once per frame
        void Update()
        {
            if (canLocomote)
            {
                // Check if player side translation reached left and right limits
                CheckSideTranslation();
            }
        }


        //---------------------------
        // Write something here.
        //---------------------------

        void CheckSideTranslation()
        {
            if (canGoLeft)
            {
                // translate to the sides
                transform.Translate(-Vector3.right * sideSpeed * Time.deltaTime);

                // if reached the position along the x axis
                if (transform.position.x <= sideLocomotion.x)
                {
                    canGoLeft = false;

                    //Always on the ground
                    Player.instance.isGrounded = true;

                    // resume reading inputs again
                    Player.instance.playerInput.canReadInput = true;
                }
            }

            if (canGoRight)
            {
                // translate to the sides
                transform.Translate(Vector3.right * sideSpeed * Time.deltaTime);

                // if reached the position along the x axis
                if (transform.position.x >= sideLocomotion.x)
                {
                    canGoRight = false;

                    //Always on the ground
                    Player.instance.isGrounded = true;

                    // resume reading inputs again
                    Player.instance.playerInput.canReadInput = true;
                }
            }
        }

        //---------------------------
        // Write something here.
        //---------------------------

        public void LocomoteToSides(SideDirection dir)
        {
            currentPath += (int)dir;

            if (dir == SideDirection.Right)
            {
                // if exist this path when going to right
                if (paths.TryGetValue(currentPath, out sideLocomotion))
                {
                    // enable right locomotion
                    canGoRight = true;

                    // gets the vector position of going right
                    sideLocomotion = paths[currentPath];
                }
                else
                {
                    // else set back to 1 which indicated is on the right limit
                    currentPath = 1;

                    // life loses energy going beyong the right limit
                    Player.instance.lifeEnergy -= 50;

                    // resume reading inputs again
                    Player.instance.playerInput.canReadInput = true;
                }
            }
            else if (dir == SideDirection.Left)
            {
                // if exist this path when going to the left
                if (paths.TryGetValue(currentPath, out sideLocomotion))
                {
                    // enable left locomotion
                    canGoLeft = true;

                    // gets the vector position of going left
                    sideLocomotion = paths[currentPath];
                }
                else
                {
                    // else set back to 1 which indicated is on the left limit
                    currentPath = -1;

                    // life loses energy going beyong the right limit
                    Player.instance.lifeEnergy -= 50;

                    // resume reading inputs again
                    Player.instance.playerInput.canReadInput = true;
                }
            }
        }

        //---------------------------
        // Write something here.
        //---------------------------

        public void Jump()
        {
            Player.instance.rigidBody.AddForce(transform.up * jumpForce);
        }
    }
}


